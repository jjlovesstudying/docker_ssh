#!/bin/bash

if [ $(id -u) -ne 0 ]; then
   echo "Only root may add user"
   exit 0
fi

read -p "Enter username: " my_user
read -p "Enter UID: " my_uid
read -p "Enter GID: " my_gid

addgroup --gid $my_gid $my_user
useradd -m -u $my_uid -g $my_gid -s /bin/bash -p "$(openssl passwd -1 dockpass001)" $my_user
usermod -aG sudo $my_user
mkdir /home/$my_user/docker_mnt
chown $my_user:$my_user /home/$my_user
chown $my_user:$my_user /home/$my_user/docker_mnt

