# ------------------------------------------------------------------------------------------------------------
# Ref: https://medium.com/faun/set-current-host-user-for-docker-container-4e521cef9ffc
# Careful: After every FROM, all ARGS gets collected and no longer available
# Command 1:
#     docker build -t oia/myimage .
# Command 2:
#     docker run -itd --entrypoint entrypoint.sh -v /home/yjj/docker_mnt:/home/user1/docker_mnt --name mycontainer oia/myimage /bin/bash
#     docker exec -it mycontainer oia_adduser.sh
# Command 3: (optional)
#     docker exec -it mycontainer /bin/bash
# Command 4:
#     docker inspect mycontainer  -> get the ip address
#     ssh user1@ip_address        -> default password is dockpass001
#     passwd user1                -> user to change his password 
# ------------------------------------------------------------------------------------------------------------

# --------------------
# Change accordingly
# --------------------
ARG IMAGE_NAME=ubuntu

FROM ${IMAGE_NAME}


# ------------------------------
# Step 1: Installing libraries
# ------------------------------
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        openssl openssh-server \
        sudo \
        nano \
        vim  \
    && rm -rf /var/lib/apt/lists/*

# ------------------------------
# Step 2: Adding user and file
# -----------------------------
COPY entrypoint.sh  /usr/local/bin/entrypoint.sh
COPY oia_adduser.sh /usr/local/bin/oia_adduser.sh

RUN chmod 700 /usr/local/bin/entrypoint.sh  && \
    chmod 700 /usr/local/bin/oia_adduser.sh

# -----------------------
# Step 3: Setting up SSH
# -----------------------
RUN mkdir /var/run/sshd && \
    sed -i 's/#*PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config && \
    sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' /etc/pam.d/sshd && \
    echo "export VISIBLE=now" >> /etc/profile


ENTRYPOINT ["entrypoint.sh"]
