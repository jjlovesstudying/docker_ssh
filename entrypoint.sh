#!/bin/bash

# Run SSH service
/usr/sbin/sshd

# Extra line added in the script to run all command line arguments
exec "$@";
